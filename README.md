# Onework Auth Service
<img src="./logo/onework.svg" align="right" width="245">

## Summary

* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
* [Running](#running)

## Description 

The `onework-auth-service` is responsible for manipulating users sign-in, sign-up and etc...

## Dependencies

[GoLang (v1.20.3)](https://golang.org/) - Go is an open source programming language that makes it easy to build simple, reliable and efficient software.

Loading and updating packages:
```bash 
$ make tidy
```

## Linter

Running project linter and checking:
```bash
$ make lint
```

## Setup

Running `docker-compose.yml` up and creating development environment:
```bash
$ make local
```

* Postgres: http://localhost:5432*
* Redis: http://localhost:6379*

Pulling proto files:
```bash
$ make pull-sub-module
```

Generating proto files:
```bash
$ make proto-gen
```

Running `docker-compose` down and stopping local services:
```bash 
$ make down
```

## Environment Variables
```bash
$ touch ./.env
$ cp sample.env .env
```
Open .env file, fix it as writen

## Database 

Creating structure:
```bash 
$ make migrateup
```

Rolling back structure:
```bash
$ make migratedown
```

## Running 

Starting project:
```bash
$ make run
```