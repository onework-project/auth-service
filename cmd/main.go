package main

import (
	"fmt"
	"net"

	"github.com/go-redis/redis/v9"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/onework-project/auth-service/config"
	pb "gitlab.com/onework-project/auth-service/genproto/auth_service"
	grpcPkg "gitlab.com/onework-project/auth-service/pkg/grpc_client"
	"gitlab.com/onework-project/auth-service/pkg/logging"
	"gitlab.com/onework-project/auth-service/pkg/token"
	"gitlab.com/onework-project/auth-service/service"
	"gitlab.com/onework-project/auth-service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load(".")
	logging.Init()
	log := logging.GetLogger()

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.RedisAddr,
	})

	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)

	grpcConn, err := grpcPkg.New(cfg)
	if err != nil {
		log.Fatalf("failed to get grpc connections: %v\n", err)
	}
	log.Println(len(cfg.TokenSymmetricKey))
	maker, err := token.NewJWTMaker(cfg.TokenSymmetricKey)
	if err != nil {
		log.Fatalf("failed to get new paseto maker %v", err)
	}

	userService := service.NewUserService(strg, inMemory, &log)
	sessionService := service.NewSessionService(strg, &log)
	authService := service.NewAuthService(&service.AuthServiceOptions{
		Strg:       strg,
		InMemory:   inMemory,
		Grpc:       grpcConn,
		Cfg:        &cfg,
		Log:        &log,
		TokenMaker: maker,
	})

	listen, err := net.Listen("tcp", cfg.GrpcPort)

	s := grpc.NewServer()
	pb.RegisterUserServiceServer(s, userService)
	pb.RegisterSessionServiceServer(s, sessionService)
	pb.RegisterAuthServiceServer(s, authService)
	reflection.Register(s)

	log.Println("gRPC server started port in: ", cfg.GrpcPort)
	if s.Serve(listen); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
