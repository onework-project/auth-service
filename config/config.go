package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

type Config struct {
	GrpcPort                    string
	Postgres                    PostgresConfig
	TokenSymmetricKey           string
	AccessTokenDuration         time.Duration
	RefreshTokenDuration        time.Duration
	RedisAddr                   string
	NotificationServiceHost     string
	NotificationServiceGrpcPort string
	ApplicantServiceHost        string
	ApplicantServiceGrpcPort    string
}

type PostgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

func Load(path string) Config {
	godotenv.Load(path + "/.env")

	conf := viper.New()
	conf.AutomaticEnv()

	cfg := Config{
		GrpcPort: conf.GetString("GRPC_PORT"),
		Postgres: PostgresConfig{
			Host:     conf.GetString("POSTGRES_HOST"),
			Port:     conf.GetString("POSTGRES_PORT"),
			User:     conf.GetString("POSTGRES_USER"),
			Password: conf.GetString("POSTGRES_PASSWORD"),
			Database: conf.GetString("POSTGRES_DATABASE"),
		},
		AccessTokenDuration:         conf.GetDuration("ACCESS_TOKEN_DURATION"),
		RefreshTokenDuration:        conf.GetDuration("REFRESH_TOKEN_DURATION"),
		TokenSymmetricKey:           conf.GetString("TOKEN_SYMMETRIC_KEY"),
		RedisAddr:                   conf.GetString("REDIS_ADDR"),
		NotificationServiceHost:     conf.GetString("NOTIFICATION_SERVICE_HOST"),
		NotificationServiceGrpcPort: conf.GetString("NOTIFICATION_SERVICE_GRPC_PORT"),
		ApplicantServiceHost:        conf.GetString("APPLICANT_SERVICE_HOST"),
		ApplicantServiceGrpcPort:    conf.GetString("APPLICANT_SERVICE_GRPC_PORT"),
	}
	return cfg
}
