create table if not exists "users" (
    "id" serial primary key,
    "email" varchar(100) not null unique,
    "password" varchar not null, -- * password should be hashed
    "type" varchar(10) not null check ("type" in ('superadmin', 'applicant', 'member')),
    "company_id" int check ("company_id" > 0),
    "lang" varchar(10) not null check ("lang" in ('uz', 'ru', 'eng')), -- langauge default should be uz
    "created_at" timestamp with time zone default current_timestamp
);

INSERT INTO users (email, password, type, lang) VALUES ('zohidsaidov17+superadmin@gmail.com', '$2a$04$B8ZFc2NclbtolJw/wbOQkubjwpnoJLTQkI6evSHjXarEufDjooAYW', 'superadmin', 'uz');
INSERT INTO users (email, password, type, lang) VALUES ('zohidsaidov17+member@gmail.com', '$2a$04$B8ZFc2NclbtolJw/wbOQkubjwpnoJLTQkI6evSHjXarEufDjooAYW', 'member', 'eng');

-- refresh token -> eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImQ5MTc0MTM1LTczNDYtNGMwZS04YjkxLWRlYmFkNjYxZTU2NSIsInVzZXJfaWQiOjMsImVtYWlsIjoiem9oaWRzYWlkb3YxNythcHBsaWNhbnRAZ21haWwuY29tIiwidXNlcl90eXBlIjoiYXBwbGljYW50IiwiaXNzdWVkX2F0IjoiMjAyMy0wMy0yNlQwNjozNzowOC4xNTMxMDcyMjgrMDU6MDAiLCJleHBpcmVkX2F0IjoiMjAyMy0wMy0yN1QwNjozNzowOC4xNTMxMDcyODErMDU6MDAifQ.VAIeuIGLCj6MXcCmSTCrOIxzbRkw5ldF2Mf_7xkl-8U
INSERT INTO users (email, password, type, lang) VALUES ('zohidsaidov17+applicant@gmail.com', '$2a$04$B8ZFc2NclbtolJw/wbOQkubjwpnoJLTQkI6evSHjXarEufDjooAYW', 'applicant', 'ru');