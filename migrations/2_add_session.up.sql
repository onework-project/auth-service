create table if not exists "sessions" (
    "id" uuid primary key,
    "email" varchar(100) not null references users(email) ON DELETE CASCADE,
    "refresh_token" varchar not null,
    "user_agent" varchar not null,
    "client_ip" varchar not null,
    "is_blocked" boolean not null default false,
    "expires_at" timestamp not null,
    "created_at" timestamp with time zone default current_timestamp
);