-- * Applicant Permissions
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'images', 'upload');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'pdfs', 'upload');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'pdfs', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'images', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicants', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicants', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicants', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicants', 'update-step');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_medias', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_skills', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_languages', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_academics', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_academics', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_academics', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_awards', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_awards', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_awards', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_educations', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_educations', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_educations', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_licences', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_licences', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_licences', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_researches', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_researches', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_researches', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_blocked_company', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_blocked_company', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_saved_jobes', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_saved_jobes', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_steps', 'update');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'auth', 'logout');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applications', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applications', 'get');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applications', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applications', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'email', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'email', 'verify');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'password', 'update');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'language', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'profile-applicant', 'get');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'chats', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'chats', 'getall');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'chats', 'get');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'messages', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'session', 'terminate');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'session', 'getall');

-- * Member Permissions 
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'images', 'upload');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'images', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'applicants', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies_blocked_applicants', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies_blocked_applicants', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'companies_media', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'members', 'add');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'members', 'get');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'members', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'members', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'members', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies', 'get-all-disactive');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies', 'activate-or-deactivate');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies_languages', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies_markets', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'vacancies_skills', 'create');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'auth', 'logout');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'applications', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'applications', 'reject');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'applications', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'email', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'email', 'verify');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'users', 'get-by-email');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'users', 'delete');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'language', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'profile-member', 'get');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'chats', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'chats', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'chats', 'getall');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'chats', 'get');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'messages', 'getall');

INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'session', 'terminate');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('member', 'session', 'getall');