-- * Applicant Permissions
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_works', 'create');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_works', 'update');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applicant_works', 'delete');
INSERT INTO "permissions" (user_type, resource, action) VALUES ('applicant', 'applications', 'check');