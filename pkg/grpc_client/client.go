package grpc_client

import (
	"fmt"

	"gitlab.com/onework-project/auth-service/config"
	pbs "gitlab.com/onework-project/auth-service/genproto/applicant_service"
	pbn "gitlab.com/onework-project/auth-service/genproto/notification_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcClientI interface {
	NotificationService() pbn.NotificationServiceClient
	ApplicantStepService() pbs.StepServiceClient
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	conNotificationService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.NotificationServiceHost, cfg.NotificationServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("notification service dial host: %s port %s err: %v", cfg.NotificationServiceHost, cfg.NotificationServiceGrpcPort, err)
	}
	conApplicantStepService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("applicant service dial host: %s port %s err: %v", cfg.ApplicantServiceHost, cfg.ApplicantServiceGrpcPort, err)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"notification_service":   pbn.NewNotificationServiceClient(conNotificationService),
			"applicant_step_service": pbs.NewStepServiceClient(conApplicantStepService),
		},
	}, nil
}

func (g *GrpcClient) NotificationService() pbn.NotificationServiceClient {
	return g.connections["notification_service"].(pbn.NotificationServiceClient)
}

func (g *GrpcClient) ApplicantStepService() pbs.StepServiceClient {
	return g.connections["applicant_step_service"].(pbs.StepServiceClient)
}
