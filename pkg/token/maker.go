package token

// Maker is an interface for managing tokens
type Maker interface {
	// CreateToken created  new token for a specific params and duration
	CreateToken(tokenParams *TokenParams) (string, *Payload, error)

	// Verify Token  check id the token is valid or not
	VerifyToken(token string) (*Payload, error)
}