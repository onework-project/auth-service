package service

// TODO: error handling
import (
	"context"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitlab.com/onework-project/auth-service/config"
	pb "gitlab.com/onework-project/auth-service/genproto/auth_service"
	"gitlab.com/onework-project/auth-service/genproto/notification_service"
	grpcPkg "gitlab.com/onework-project/auth-service/pkg/grpc_client"
	"gitlab.com/onework-project/auth-service/pkg/logging"
	"gitlab.com/onework-project/auth-service/pkg/token"
	"gitlab.com/onework-project/auth-service/pkg/utils"
	"gitlab.com/onework-project/auth-service/storage"
	"gitlab.com/onework-project/auth-service/storage/repo"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type AuthService struct {
	pb.UnimplementedAuthServiceServer
	storage    storage.StorageI
	inMemory   storage.InMemoryStorageI
	grpcClient grpcPkg.GrpcClientI
	cfg        *config.Config
	logger     *logging.Logger
	tokenMaker token.Maker
}

type AuthServiceOptions struct {
	Strg       storage.StorageI
	InMemory   storage.InMemoryStorageI
	Grpc       grpcPkg.GrpcClientI
	Cfg        *config.Config
	Log        *logging.Logger
	TokenMaker token.Maker
}

func NewAuthService(opts *AuthServiceOptions) *AuthService {
	return &AuthService{
		storage:    opts.Strg,
		inMemory:   opts.InMemory,
		grpcClient: opts.Grpc,
		cfg:        opts.Cfg,
		logger:     opts.Log,
		tokenMaker: opts.TokenMaker,
	}
}

const (
	RegisterCodeKey   = "register_code_"
	ForgotPasswordKey = "forgot_password_code_"
	UpdateEmailKey    = "update_email_code"
)

const (
	VerificationEmail   = "verification_email"
	ForgotPasswordEmail = "forgot_password_email"
	UpdateEmail         = "update_email"
)

func (s *AuthService) Register(ctx context.Context, req *pb.RegisterReq) (*emptypb.Empty, error) {
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		s.logger.WithError(err).Error("failed to hash password")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	user := repo.User{
		Email:    req.Email,
		Password: hashedPassword,
		Type:     req.UserType,
	}

	userData, err := json.Marshal(user)
	if err != nil {
		s.logger.WithError(err).Error("failed to marshal user in register func")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	err = s.inMemory.Set("user_"+req.Email, string(userData), 10*time.Minute)
	if err != nil {
		s.logger.WithError(err).Error("failed to set user to redis in register func")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	go func() {
		err := s.sendVereficationCode(RegisterCodeKey, req.Email, VerificationEmail)
		if err != nil {
			s.logger.WithError(err).Error("failed to send verification code in register func")
		}
	}()

	return &emptypb.Empty{}, nil
}

func (s *AuthService) sendVereficationCode(key, email, email_type string) error {
	code, err := utils.GenerateRandomCode(6)
	if err != nil {
		s.logger.WithError(err).Error("failed to generate random code")
		return err
	}
	hashedCode, err := utils.HashPassword(code)
	if err != nil {
		s.logger.WithError(err).Error("failed to hash password")
		return err
	}
	err = s.inMemory.Set(key+email, hashedCode, time.Hour)
	if err != nil {
		s.logger.WithError(err).Error("failed to send generated code in sendVerificationCode func")
		return err
	}
	_, err = s.grpcClient.NotificationService().SendEmail(context.Background(), &notification_service.SendEmailRequest{
		To:      email,
		Subject: "Verification Email",
		Body: map[string]string{
			"code": code,
		},
		Type: email_type,
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *AuthService) VerifyEmail(ctx context.Context, req *pb.VerifyReq) (*pb.TokenRes, error) {
	userData, err := s.inMemory.Get("user_" + req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user data from redis")
		return nil, status.Errorf(codes.Internal, "internale server error: %v", err)
	}
	var user repo.User
	err = json.Unmarshal([]byte(userData), &user)
	if err != nil {
		s.logger.WithError(err).Error("failed to unmarshal user data")
		return nil, status.Errorf(codes.Internal, "internal server errror: %v", err)
	}

	code, err := s.inMemory.Get(RegisterCodeKey + user.Email)
	if err != nil {
		return nil, status.Errorf(codes.DeadlineExceeded, "verification code is expired")
	}
	err = utils.CheckPassword(req.Code, code)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "failed to check password")
	}
	result, err := s.storage.User().Create(&user)
	if err != nil {
		s.logger.WithError(err).Error("failed to create user")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	accessToken, _, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   result.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: s.cfg.AccessTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create paseto token")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	refreshToken, refreshPayload, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   result.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: s.cfg.RefreshTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create paseto token")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	_, err = s.storage.Session().Create(&repo.Session{
		ID:           refreshPayload.Id,
		Email:        req.Email,
		RefreshToken: refreshToken,
		UserAgent:    req.UserAgent,
		ClientIP:     req.ClientIp,
		IsBlocked:    false,
		ExpiresAt:    refreshPayload.ExpiredAt,
		FcmToken:     req.FcmToken,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create session")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.TokenRes{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}

func (s *AuthService) Login(ctx context.Context, req *pb.LoginReq) (*pb.TokenResponse, error) {
	user, err := s.storage.User().GetByEmail(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user by email")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "user not found: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}
	err = utils.CheckPassword(req.Password, user.Password)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "incorrect_password")
	}
	accessToken, _, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   user.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: s.cfg.AccessTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create paseto token")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	refreshToken, refreshPayload, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   user.ID,
		Email:    user.Email,
		UserType: user.Type,
		Duration: s.cfg.RefreshTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create jwt token")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	_, err = s.storage.Session().Create(&repo.Session{
		ID:           refreshPayload.Id,
		Email:        req.Email,
		RefreshToken: refreshToken,
		UserAgent:    req.UserAgent,
		ClientIP:     req.ClientIp,
		IsBlocked:    false,
		ExpiresAt:    refreshPayload.ExpiredAt,
		FcmToken:     req.FcmToken,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create session")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.TokenResponse{
		UserId:        user.ID,
		UserType:      user.Type,
		AccessToken:   accessToken,
		RefreshToken:  refreshToken,
		HasPermission: true,
	}, nil
}

func (s *AuthService) ForgotPassword(ctx context.Context, req *pb.EmailRequest) (*emptypb.Empty, error) {
	_, err := s.storage.User().GetByEmail(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("user does not exists")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "email does not exists: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	go func() {
		err := s.sendVereficationCode(ForgotPasswordKey, req.Email, ForgotPasswordEmail)
		if err != nil {
			fmt.Printf("failed to send verification code: %v", err)
		}
	}()

	return &emptypb.Empty{}, nil
}

func (s *AuthService) VerifyForgotPassword(ctx context.Context, req *pb.VerifyReq) (*pb.AccessTokenRes, error) {
	code, err := s.inMemory.Get(ForgotPasswordKey + req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to get code from redis in VerifyForgoPasword func")
		return nil, status.Error(codes.DeadlineExceeded, "verification code is expired")
	}

	err = utils.CheckPassword(req.Code, code)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "failed to check password")
	}

	result, err := s.storage.User().GetByEmail(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user info by email")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "user not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	accessToken, _, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   result.ID,
		Email:    result.Email,
		UserType: result.Type,
		Duration: s.cfg.AccessTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create paseto access token")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.AccessTokenRes{
		AccessToken: accessToken,
	}, nil
}

func (s *AuthService) UpdatePassword(ctx context.Context, req *pb.UpdatePasswordReq) (*emptypb.Empty, error) {
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		s.logger.WithError(err).Error("failed to hashpassword")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	err = s.storage.User().UpdatePassword(&repo.UpdatePassword{
		UserID:   req.UserId,
		Password: hashedPassword,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update password")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &emptypb.Empty{}, nil
}

func (s *AuthService) UpdateEmail(ctx context.Context, req *pb.UpdateEmailReq) (*emptypb.Empty, error) {
	user := repo.User{
		Email: req.Email,
		ID:    req.Id,
	}

	userData, err := json.Marshal(user)
	if err != nil {
		s.logger.WithError(err).Error("failed to marshal user")
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	err = s.inMemory.Set("user_"+req.Email, string(userData), time.Hour)
	if err != nil {
		s.logger.WithError(err).Error("failed to set user to redis")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	go func() {
		err := s.sendVereficationCode(UpdateEmailKey, req.Email, UpdateEmail)
		if err != nil {
			s.logger.WithError(err).Error("failed to send verification code")
		}
	}()

	return &emptypb.Empty{}, nil
}

func (s *AuthService) VerifyUpdateEmail(ctx context.Context, req *pb.VerifyEmailReq) (*pb.User, error) {
	userData, err := s.inMemory.Get("user_" + req.NewEmail)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user data from redis")
		return nil, status.Errorf(codes.Internal, "internale server error: %v", err)
	}
	var user repo.User
	err = json.Unmarshal([]byte(userData), &user)
	if err != nil {
		s.logger.WithError(err).Error("failed to unmarshal user data")
		return nil, status.Errorf(codes.Internal, "internal server errror: %v", err)
	}

	code, err := s.inMemory.Get(UpdateEmailKey + req.NewEmail)
	if err != nil {
		s.logger.WithError(err).Error("failed to get code from redis")
		return nil, status.Error(codes.DeadlineExceeded, "verification code is expired")
	}

	err = utils.CheckPassword(req.Code, code)
	if err != nil {
		return nil, status.Errorf(codes.Unknown, "failed to check password")
	}

	err = s.storage.Session().Delete(req.OldEmail, req.ClientIp)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete session email")
		return nil, status.Errorf(codes.Internal, "failed to update email")
	}

	u, err := s.storage.User().UpdateEmail(&repo.User{
		ID:    user.ID,
		Email: user.Email,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to update email")
		return nil, status.Errorf(codes.Internal, "failed to update email")
	}

	return &pb.User{
		Id:        u.ID,
		Email:     u.Email,
		Type:      u.Type,
		CreatedAt: u.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *AuthService) VerifyToken(ctx context.Context, req *pb.VerifyTokenReq) (*pb.VerifyTokenRes, error) {
	accessToken := req.AccessToken

	payload, err := s.tokenMaker.VerifyToken(accessToken)
	if err != nil {
		s.logger.WithError(err).Error("failed to verify token")
		return nil, status.Errorf(codes.Unauthenticated, "invalid token: %v", err)
	}

	permission, err := s.storage.Permission().CheckPermission(&repo.Permission{
		UserType: payload.UserType,
		Resource: req.Resource,
		Action:   req.Action,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to verify token")
		return nil, status.Errorf(codes.Unauthenticated, "invalid token: %v", err)
	}
	if payload.UserType == repo.UserTypeSuperadmin {
		permission = true
	}
	return &pb.VerifyTokenRes{
		Id:            payload.Id.String(),
		UserId:        payload.UserID,
		Email:         payload.Email,
		UserType:      payload.UserType,
		IssuedAt:      payload.IssuedAt.Format(time.RFC3339),
		ExpiredAt:     payload.ExpiredAt.Format(time.RFC3339),
		HasPermission: permission,
	}, nil
}

func (s *AuthService) RefreshToken(ctx context.Context, req *pb.RefreshTokenReq) (*pb.AccessTokenRes, error) {
	refreshPayload, err := s.tokenMaker.VerifyToken(req.RefreshToken)
	if err != nil {
		s.logger.WithError(err).Errorf("failed to verify refresh token")
		return nil, status.Errorf(codes.Unauthenticated, "invalid token: %v", err)
	}

	session, err := s.storage.Session().Get(refreshPayload.Id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			s.logger.WithError(err).Errorf("failed to get session: %v", err)
			return nil, status.Errorf(codes.NotFound, "session not found: %v", err)
		}
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	if session.IsBlocked {
		s.logger.WithError(err).Errorf("session is blocked: %v", session.ClientIP)
		return nil, status.Errorf(codes.Unavailable, "blocked session")
	}

	if session.Email != refreshPayload.Email {
		s.logger.WithError(err).Errorf("icorrect session email")
		return nil, status.Errorf(codes.Unavailable, "incorrect session user")
	}

	if session.RefreshToken != req.RefreshToken {
		s.logger.WithError(err).Errorf("mismatched session token")
		return nil, status.Errorf(codes.Unavailable, "mismatched session token")
	}

	if time.Now().After(session.ExpiresAt) {
		s.logger.WithError(err).Errorf("expired session")
		return nil, status.Errorf(codes.Unavailable, "expired session")
	}

	accessToken, _, err := s.tokenMaker.CreateToken(&token.TokenParams{
		UserID:   refreshPayload.UserID,
		Email:    refreshPayload.Email,
		UserType: refreshPayload.UserType,
		Duration: s.cfg.AccessTokenDuration,
	})
	if err != nil {
		s.logger.WithError(err).Errorf("internal server errror while creating access token")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.AccessTokenRes{
		AccessToken: accessToken,
	}, nil
}
