package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"github.com/google/uuid"
	pb "gitlab.com/onework-project/auth-service/genproto/auth_service"
	"gitlab.com/onework-project/auth-service/pkg/logging"
	"gitlab.com/onework-project/auth-service/storage"
	"gitlab.com/onework-project/auth-service/storage/repo"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SessionService struct {
	pb.UnimplementedSessionServiceServer
	storage storage.StorageI
	logger  *logging.Logger
}

func NewSessionService(strg storage.StorageI, log *logging.Logger) *SessionService {
	return &SessionService{
		storage: strg,
		logger:  log,
	}
}

func (s *SessionService) Get(ctx context.Context, req *pb.UUIDReq) (*pb.SessionRes, error) {
	uuid, err := uuid.Parse(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to parse uuid")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}
	session, err := s.storage.Session().Get(uuid)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user in get func")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.SessionRes{
		Id:           session.ID.String(),
		Email:        session.Email,
		RefreshToken: session.RefreshToken,
		UserAgent:    session.UserAgent,
		ClientIp:     session.ClientIP,
		IsBlocked:    session.IsBlocked,
		ExpiresAt:    session.ExpiresAt.Format(time.RFC3339),
		CreatedAt:    session.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *SessionService) Delete(ctx context.Context, req *pb.DeleteSessionReq) (*emptypb.Empty, error) {
	err := s.storage.Session().Delete(req.Email, req.ClientIp)
	if err != nil {
		s.logger.WithError(err).Error("failed_to_delete")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "session_not_found")
		}
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}

	return &emptypb.Empty{}, nil
}

func (s *SessionService) BlockSession(ctx context.Context, req *pb.EmailReq) (*emptypb.Empty, error) {
	err := s.storage.Session().BlockSession(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to block or unblock session")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "session not found")
		}
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}

	return &emptypb.Empty{}, nil
}

func (s *SessionService) UnBlockSession(ctx context.Context, req *pb.EmailReq) (*emptypb.Empty, error) {
	err := s.storage.Session().UnBlockSession(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to unblock session")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "session not found")
		}
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}

	return &emptypb.Empty{}, nil
}

func (s *SessionService) GetAllFcmToken(ctx context.Context, req *pb.EmailReq) (*pb.FcmTokenRes, error) {
	fcmTokens, err := s.storage.Session().GetAllFcmToken(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to get all session fcm tokens")
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}

	var tokens pb.FcmTokenRes
	tokens.FcmTokens = append(tokens.FcmTokens, fcmTokens...)

	return &tokens, nil
}

func (s *SessionService) TerminateSession(ctx context.Context, req *pb.UUIDReq) (*emptypb.Empty, error) {
	err := s.storage.Session().DeleteWithSessionId(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed_to_delete")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "session_not_found")
		}
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}

	return &emptypb.Empty{}, nil
}

func (s *SessionService) GetAllSessions(ctx context.Context, req *pb.GetAllSessionsParamsReq) (*pb.GetAllSessionsRes, error) {
	sessions, err := s.storage.Session().GetAll(&repo.GetAllSessionsParams{
		Limit: req.Limit,
		Page:  req.Page,
		Email: req.Email,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all sessions")
		return nil, status.Errorf(codes.Internal, "internal_server_error")
	}
	sessions2 := pb.GetAllSessionsRes{
		Count: sessions.Count,
	}
	for _, v := range sessions.Sessions {
		sessions2.Sessions = append(sessions2.Sessions, &pb.Session{
			Id:        v.ID.String(),
			Email:     v.Email,
			UserAgent: v.UserAgent,
			ClientIp:  v.ClientIP,
			IsBlocked: v.IsBlocked,
			CreatedAt: v.CreatedAt.Format(time.RFC3339),
		})
	}

	return &sessions2, nil
}
