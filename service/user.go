package service

import (
	"context"
	"database/sql"
	"errors"
	"time"

	pb "gitlab.com/onework-project/auth-service/genproto/auth_service"
	"gitlab.com/onework-project/auth-service/pkg/logging"
	"gitlab.com/onework-project/auth-service/pkg/utils"
	"gitlab.com/onework-project/auth-service/storage"
	"gitlab.com/onework-project/auth-service/storage/repo"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type UserService struct {
	pb.UnimplementedUserServiceServer
	storage  storage.StorageI
	inMemory storage.InMemoryStorageI
	logger   *logging.Logger
}

func NewUserService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logging.Logger) *UserService {
	return &UserService{
		storage:  strg,
		inMemory: inMemory,
		logger:   log,
	}
}

func (s *UserService) Create(ctx context.Context, req *pb.CreateUser) (*pb.User, error) {
	hashedPassword, err := utils.HashPassword(req.Password)
	if err != nil {
		s.logger.WithError(err).Error("failed to hash password")
		return nil, status.Errorf(codes.Internal, "internatl server error: %v", err)
	}
	user, err := s.storage.User().Create(&repo.User{
		Email:    req.Email,
		Password: hashedPassword,
		Type:     req.Type,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to create user")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.User{
		Id:        user.ID,
		Email:     user.Email,
		Type:      user.Type,
		Language:  user.Language,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *UserService) AddCompanyId(ctx context.Context, req *pb.AddCompanyIdRes) (*emptypb.Empty, error) {
	err := s.storage.User().AddCompanyID(&repo.User{
		ID:        req.Id,
		CompanyID: req.CompanyId,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to add company id")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &emptypb.Empty{}, nil
}

func (s *UserService) DeleteCompanyID(ctx context.Context, req *pb.GetByIdRequest) (*emptypb.Empty, error) {
	err := s.storage.User().DeleteCompanyID(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete company id")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &emptypb.Empty{}, nil
}

func (s *UserService) Get(ctx context.Context, req *pb.GetByIdRequest) (*pb.User, error) {
	user, err := s.storage.User().Get(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to get user in get func")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &pb.User{
		Id:        user.ID,
		Email:     user.Email,
		Type:      user.Type,
		CompanyId: user.CompanyID,
		Language:  user.Language,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *UserService) GetByEmail(ctx context.Context, req *pb.GetByEmailRequest) (*pb.User, error) {
	user, err := s.storage.User().GetByEmail(req.Email)
	if err != nil {
		s.logger.WithError(err).Error("failed to getbyemail user in get func")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return &pb.User{
		Id:        user.ID,
		Email:     user.Email,
		Type:      user.Type,
		CompanyId: user.CompanyID,
		Language:  user.Language,
		CreatedAt: user.CreatedAt.Format(time.RFC3339),
	}, nil
}

func (s *UserService) Delete(ctx context.Context, req *pb.GetByIdRequest) (*emptypb.Empty, error) {
	err := s.storage.User().Delete(req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to delete user")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "user_not_exists")
		}
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}
	return &emptypb.Empty{}, nil
}

func (s *UserService) GetAll(ctx context.Context, req *pb.GetAllUsersRequest) (*pb.GetAllUsersResponse, error) {
	users, err := s.storage.User().GetAll(&repo.GetAllUserParams{
		Limit:  req.Limit,
		Page:   req.Page,
		Search: req.Search,
	})
	if err != nil {
		s.logger.WithError(err).Error("failed to get all user in getall func")
		return nil, status.Errorf(codes.Internal, "internal server error: %v", err)
	}

	return parseUserResponse(users)
}

func (s *UserService) UpdateLanguage(ctx context.Context, req *pb.UpdateLanguageReq) (*emptypb.Empty, error) {
	err := s.storage.User().UpdateLanguage(req.Lang, req.Id)
	if err != nil {
		s.logger.WithError(err).Error("failed to update language")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, "not found")
		}
		return nil, status.Errorf(codes.Internal, "internal server error")
	}

	return &emptypb.Empty{}, nil
}

func parseUserResponse(users *repo.GetAllUsersResult) (*pb.GetAllUsersResponse, error) {
	res := pb.GetAllUsersResponse{
		Users: make([]*pb.User, 0),
		Count: users.Count,
	}
	for _, user := range users.Users {
		u := pb.User{
			Id:        user.ID,
			Email:     user.Email,
			Type:      user.Type,
			CompanyId: user.CompanyID,
			Language:  user.Language,
			CreatedAt: user.CreatedAt.Format(time.RFC3339),
		}
		res.Users = append(res.Users, &u)
	}

	return &res, nil
}
