package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/auth-service/storage/repo"
)

type sessionRepo struct {
	db *sqlx.DB
}

func NewSession(db *sqlx.DB) repo.SessionStorageI {
	return &sessionRepo{
		db: db,
	}
}

func (sr *sessionRepo) Create(s *repo.Session) (*repo.Session, error) {
	_, err := getByEmail(sr.db, s.Email, s.ClientIP)
	if errors.Is(err, sql.ErrNoRows) {
		query := `
			INSERT INTO sessions (
				id,
				email,
				refresh_token,
				user_agent,
				client_ip,
				is_blocked,
				expires_at,
				fcm_token
			) VALUES ($1,$2,$3,$4,$5,$6,$7,$8)
			RETURNING created_at
		`

		err := sr.db.QueryRow(
			query,
			s.ID,
			s.Email,
			s.RefreshToken,
			s.UserAgent,
			s.ClientIP,
			s.IsBlocked,
			s.ExpiresAt,
			s.FcmToken,
		).Scan(&s.CreatedAt)
		if err != nil {
			return nil, err
		}
	} else {
		query := `
			UPDATE sessions SET
				id = $1,
				email = $2,
				refresh_token = $3,
				user_agent = $4,
				client_ip = $5,
				is_blocked = $6,
				expires_at = $7,
				fcm_token = $8,
				created_at = CURRENT_TIMESTAMP 
			WHERE email = $9 AND client_ip = $10 RETURNING created_at
		`
		err = sr.db.QueryRow(
			query,
			s.ID,
			s.Email,
			s.RefreshToken,
			s.UserAgent,
			s.ClientIP,
			s.IsBlocked,
			s.ExpiresAt,
			s.FcmToken,
			s.Email,
			s.ClientIP,
		).Scan(&s.CreatedAt)
		if err != nil {
			return nil, err
		}
	}

	return s, nil
}

func (sr *sessionRepo) Get(id uuid.UUID) (*repo.Session, error) {
	var s repo.Session
	query := `
		SELECT
			id,
			email,
			refresh_token,
			user_agent,
			client_ip,
			is_blocked,
			expires_at,
			fcm_token,
			created_at
		FROM sessions 
		WHERE id = $1
	`

	err := sr.db.QueryRow(
		query,
		id,
	).Scan(
		&s.ID,
		&s.Email,
		&s.RefreshToken,
		&s.UserAgent,
		&s.ClientIP,
		&s.IsBlocked,
		&s.ExpiresAt,
		&s.FcmToken,
		&s.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *sessionRepo) DeleteWithSessionId(session string) error {
	query := `
		DELETE FROM sessions WHERE id = $1
	`
	res, err := sr.db.Exec(
		query,
		session,
	)
	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func getByEmail(db *sqlx.DB, email, ip string) (*repo.Session, error) {
	var s repo.Session
	query := `
		SELECT
			id
		FROM sessions 
		WHERE email = $1 AND client_ip = $2
	`

	err := db.QueryRow(
		query,
		email,
		ip,
	).Scan(
		&s.ID,
	)
	if err != nil {
		return nil, err
	}

	return &s, nil
}

func (sr *sessionRepo) Delete(email string, clientIP string) error {
	query := `
		DELETE FROM sessions WHERE email = $1 AND client_ip = $2
	`

	res, err := sr.db.Exec(
		query,
		email,
		clientIP,
	)
	if err != nil {
		return err
	}

	if result, _ := res.RowsAffected(); result == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (sr *sessionRepo) BlockSession(email string) error {
	query := `
		UPDATE sessions SET is_blocked = true WHERE email = $1
	`
	res, err := sr.db.Exec(
		query,
		email,
	)

	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (sr *sessionRepo) UnBlockSession(email string) error {
	query := `
		UPDATE sessions SET is_blocked = false WHERE email = $1
	`
	res, err := sr.db.Exec(
		query,
		email,
	)

	if err != nil {
		return err
	}
	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (sr *sessionRepo) GetAllFcmToken(email string) ([]string, error) {
	filter := ""
	if email != "" {
		filter = fmt.Sprintf(" WHERE email = %s", email)
	}
	query := `
		SELECT 
			fcm_token
		FROM sessions
	` + filter
	rows, err := sr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	tokens := make([]string, 0)
	for rows.Next() {
		var token string
		err := rows.Scan(&token)
		if err != nil {
			return nil, err
		}

		tokens = append(tokens, token)
	}

	return tokens, err
}

func (sr *sessionRepo) GetAll(params *repo.GetAllSessionsParams) (*repo.GetAllSessions, error) {
	result := repo.GetAllSessions{
		Sessions: make([]*repo.SessionRes, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""

	if params.Email != "" {
		filter = fmt.Sprintf(` WHERE email = '%v' `, params.Email)
	}

	query := `
		SELECT 
			id,
			email,
			user_agent,
			client_ip,
			is_blocked,
			created_at
		FROM sessions
	` + filter + `
		ORDER BY created_at DESC
	` + limit

	rows, err := sr.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var session repo.SessionRes
		err := rows.Scan(
			&session.ID,
			&session.Email,
			&session.UserAgent,
			&session.ClientIP,
			&session.IsBlocked,
			&session.CreatedAt,
		)
		if err != nil {
			return nil, err
		}

		result.Sessions = append(result.Sessions, &session)
	}

	queryCount := "SELECT count(1) FROM sessions " + filter

	if err = sr.db.QueryRow(queryCount).Scan(&result.Count); err != nil {
		return nil, err
	}

	return &result, nil
}
