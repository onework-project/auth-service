package postgres

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/auth-service/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

func NewUser(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{
		db: db,
	}
}

func (ur *userRepo) Create(user *repo.User) (*repo.User, error) {
	query := `
		INSERT INTO users (
			email,
			password,
			type,
			lang
		) VALUES ($1, $2, $3, $4)
		RETURNING id, email, type, lang, created_at
	`
	var res repo.User
	err := ur.db.QueryRow(
		query,
		user.Email,
		user.Password,
		user.Type,
		repo.UzbekLang,
	).Scan(
		&res.ID,
		&res.Email,
		&res.Type,
		&res.Language,
		&res.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func (ur *userRepo) Get(user_id int64) (*repo.User, error) {
	var (
		result repo.User
		id     sql.NullInt64
	)
	query := `
		SELECT 
			id,
			email,
			type,
			company_id,
			lang,
			created_at
		FROM users WHERE id = $1
	`
	err := ur.db.QueryRow(
		query,
		user_id,
	).Scan(
		&result.ID,
		&result.Email,
		&result.Type,
		&id,
		&result.Language,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	result.CompanyID = id.Int64

	return &result, nil
}

func (ur *userRepo) UpdateEmail(user *repo.User) (*repo.User, error) {
	query := `
		UPDATE users SET
			email=$1
		WHERE id=$2 
		RETURNING 
			type,
			company_id,
			lang,
			created_at
	`
	var id sql.NullInt64
	err := ur.db.QueryRow(
		query,
		user.Email,
		user.ID,
	).Scan(
		&user.Type,
		&id,
		&user.Language,
		&user.CreatedAt,
	)
	if err != nil {
		return nil, err
	}
	user.CompanyID = id.Int64

	return user, nil
}

func (ur *userRepo) AddCompanyID(user *repo.User) error {
	query := `
		UPDATE users SET
			company_id=$1
		WHERE id=$2 
	`
	res, err := ur.db.Exec(
		query,
		user.CompanyID,
		user.ID,
	)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}
	return nil
}

func (ur *userRepo) DeleteCompanyID(userId int64) error {
	query := `
		UPDATE users SET company_id = NULL WHERE id = $1
	`

	res, err := ur.db.Exec(query, userId)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *userRepo) Delete(user_id int64) error {
	query := `
		DELETE FROM users WHERE id = $1
	`
	result, err := ur.db.Exec(
		query,
		user_id,
	)
	if err != nil {
		return err
	}
	if count, _ := result.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *userRepo) GetAll(params *repo.GetAllUserParams) (*repo.GetAllUsersResult, error) {
	result := repo.GetAllUsersResult{
		Users: make([]*repo.User, 0),
	}

	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" LIMIT %d OFFSET %d", params.Limit, offset)

	filter := ""

	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter = fmt.Sprintf(`
			WHERE email ILIKE '%s'
		`, str)
	}

	query := `
		SELECT 
			id,
			email,
			type,
			company_id,
			lang,
			created_at
		FROM users
	` + filter + `
		ORDER BY created_at DESC
	` + limit

	rows, err := ur.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var user repo.User
		var id sql.NullInt64
		err := rows.Scan(
			&user.ID,
			&user.Email,
			&user.Type,
			&id,
			&user.Language,
			&user.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		user.CompanyID = id.Int64
		result.Users = append(result.Users, &user)
	}

	queryCount := "SELECT count(1) FROM users " + filter

	if err = ur.db.QueryRow(queryCount).Scan(&result.Count); err != nil {
		return nil, err
	}

	return &result, nil
}

func (ur *userRepo) GetByEmail(user_email string) (*repo.User, error) {
	var (
		result repo.User
	)

	query := `
		SELECT 
			id,
			email,
			password,
			type,
			company_id,
			lang,
			created_at
		FROM users WHERE email = $1
	`
	var id sql.NullInt64
	err := ur.db.QueryRow(
		query,
		user_email,
	).Scan(
		&result.ID,
		&result.Email,
		&result.Password,
		&result.Type,
		&id,
		&result.Language,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	result.CompanyID = id.Int64
	return &result, nil
}

func (ur *userRepo) UpdateLanguage(lang string, id int64) error {
	query := `
		UPDATE users SET lang = $1 WHERE id = $2
	`
	res, err := ur.db.Exec(query, lang, id)
	if err != nil {
		return err
	}

	if count, _ := res.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}

	return nil
}

func (ur *userRepo) UpdatePassword(req *repo.UpdatePassword) error {
	query := `UPDATE users SET password=$1 WHERE id=$2`
	_, err := ur.db.Exec(query, req.Password, req.UserID)
	if err != nil {
		return err
	}

	return nil
}
