package postgres_test

import (
	"testing"

	"github.com/bxcodec/faker/v4"
	"github.com/stretchr/testify/require"
	"gitlab.com/onework-project/auth-service/pkg/utils"
	"gitlab.com/onework-project/auth-service/storage/repo"
)

func createUser(t *testing.T) *repo.User {
	hashedPassword, err := utils.HashPassword("1234567890")
	require.NoError(t, err)
	user, err := dbManager.User().Create(&repo.User{
		Email:    faker.Email(),
		Password: hashedPassword,
		Type:     repo.UserTypeApplicant,
	})
	require.NoError(t, err)
	require.NotEmpty(t, user)
	return user
}

func deleteUser(t *testing.T, user_id int64) {
	err := dbManager.User().Delete(user_id)
	require.NoError(t, err)
}

func TestCreateUser(t *testing.T) {
	user := createUser(t)
	deleteUser(t, user.ID)
}

func TestUpdateUser(t *testing.T) {
	user := createUser(t)
	u, err := dbManager.User().UpdateEmail(&repo.User{
		ID:    user.ID,
		Email: faker.Email(),
	})
	require.NoError(t, err)
	require.NotEmpty(t, u)
	deleteUser(t, u.ID)
}

func TestDeleteUser(t *testing.T) {
	user := createUser(t)
	deleteUser(t, user.ID)
}

func TestGetUser(t *testing.T) {
	user := createUser(t)
	u, err := dbManager.User().Get(user.ID)
	require.NoError(t, err)
	require.NotEmpty(t, u)
	deleteUser(t, u.ID)
}

func TestGetAll(t *testing.T) {
	ids := [10]int64{}
	for i := 0; i < 10; i++ {
		user := createUser(t)
		ids[i] = user.ID
	}

	users, err := dbManager.User().GetAll(&repo.GetAllUserParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)
	require.GreaterOrEqual(t, len(users.Users), 10)

	for i := 0; i < 10; i++ {
		deleteUser(t, ids[i])
	}
}
