package repo

import (
	"time"

	"github.com/google/uuid"
)

type SessionStorageI interface {
	Create(s *Session) (*Session, error)
	DeleteWithSessionId(session string) error
	Get(id uuid.UUID) (*Session, error)
	Delete(email string, clientIP string) error
	BlockSession(email string) error
	UnBlockSession(email string) error
	GetAllFcmToken(email string) ([]string, error)
	GetAll(params *GetAllSessionsParams) (*GetAllSessions, error)
}

type GetAllSessionsParams struct {
	Limit int64
	Page  int64
	Email string
}

type GetAllSessions struct {
	Sessions []*SessionRes
	Count    int64
}

type SessionRes struct {
	ID        uuid.UUID
	Email     string
	UserAgent string
	ClientIP  string
	IsBlocked bool
	CreatedAt time.Time
}

type Session struct {
	ID           uuid.UUID
	Email        string
	RefreshToken string
	UserAgent    string
	ClientIP     string
	IsBlocked    bool
	ExpiresAt    time.Time
	FcmToken     string
	CreatedAt    time.Time
}
