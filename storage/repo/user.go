package repo

import "time"

const (
	UserTypeSuperadmin = "superadmin"
	UserTypeApplicant  = "applicant"
	UserTypeMember     = "member"
	UzbekLang          = "uz"
)

type User struct {
	ID        int64
	Email     string
	Password  string
	Type      string
	CompanyID int64
	Language  string
	CreatedAt time.Time
}

type UserStorageI interface {
	Create(u *User) (*User, error)
	UpdatePassword(u *UpdatePassword) error
	AddCompanyID(user *User) error
	DeleteCompanyID(userId int64) error
	Get(user_id int64) (*User, error)
	UpdateEmail(u *User) (*User, error)
	UpdateLanguage(lang string, id int64) error
	Delete(user_id int64) error
	GetAll(params *GetAllUserParams) (*GetAllUsersResult, error)
	GetByEmail(user_email string) (*User, error)
}

type UpdatePassword struct {
	UserID   int64
	Password string
}

type GetAllUserParams struct {
	Limit  int32
	Page   int32
	Search string
}

type GetAllUsersResult struct {
	Users []*User
	Count int32
}
