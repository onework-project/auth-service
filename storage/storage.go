package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/onework-project/auth-service/storage/postgres"
	"gitlab.com/onework-project/auth-service/storage/repo"
)

type StorageI interface {
	User() repo.UserStorageI
	Permission() repo.PermissionStorageI
	Session() repo.SessionStorageI
}

type StoragePg struct {
	userRepo       repo.UserStorageI
	permissionRepo repo.PermissionStorageI
	sessionRepo    repo.SessionStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &StoragePg{
		userRepo:       postgres.NewUser(db),
		permissionRepo: postgres.NewPermission(db),
		sessionRepo:    postgres.NewSession(db),
	}
}

func (s *StoragePg) User() repo.UserStorageI {
	return s.userRepo
}

func (s *StoragePg) Permission() repo.PermissionStorageI {
	return s.permissionRepo
}

func (s *StoragePg) Session() repo.SessionStorageI {
	return s.sessionRepo
}
